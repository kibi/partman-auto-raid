# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Slovak messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Peter Mann <peter.mann@tuke.sk>
# Ivan Masár <helix84@centrum.sk>, 2007, 2008, 2009, 2010, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: partman-auto-raid@packages.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:33+0000\n"
"PO-Revision-Date: 2011-03-21 02:13+0100\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: Slovak <sk-i18n@lists.linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: error
#. Description
#. :sl3:
#: ../partman-auto-raid.templates:1001
msgid "Error while setting up RAID"
msgstr "Chyba pri nastavovaní RAID"

#. Type: error
#. Description
#. :sl3:
#: ../partman-auto-raid.templates:1001
msgid ""
"An unexpected error occurred while setting up a preseeded RAID configuration."
msgstr ""
"Pri nastavovaní predvolenej RAID konfigurácie došlo k neočakávanej chybe."

#. Type: error
#. Description
#. :sl3:
#: ../partman-auto-raid.templates:1001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr ""
"Podrobnosti nájdete v súbore /var/log/syslog alebo na štvrtej virtuálnej "
"konzole."

#. Type: error
#. Description
#. :sl3:
#: ../partman-auto-raid.templates:4001
msgid "Not enough RAID partitions specified"
msgstr "Neexistuje dostatok RAID oblastí"

#. Type: error
#. Description
#. :sl3:
#: ../partman-auto-raid.templates:4001
msgid ""
"There are not enough RAID partitions specified for your preseeded "
"configuration. You need at least 3 devices for a RAID5 array."
msgstr ""
"Pre vašu zvolenú konfiguráciu neexistuje dostatok RAID oblastí. Potrebujete "
"aspoň 3 zariadenia na vytvorenie poľa RAID5."
